// ==UserScript==
// @name        Haaretz fade-out premium articles
// @namespace   amitkeret
// @version     0.1.5
// @updateURL   https://bitbucket.org/amitkeret/userscripts/raw/master/haaretz.user.js
// @downloadURL https://bitbucket.org/amitkeret/userscripts/raw/master/haaretz.user.js
// @description
// @match       http*://www.haaretz.co.il/*
// @author      amitkeret
// @copyright   amitkeret
// @icon        http://www.haaretz.co.il/htz/images/favicon-16x16.png
// @require     http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js
// @require     https://cdn.rawgit.com/hazzik/livequery/master/dist/jquery.livequery.min.js
// @grant       none
// ==/UserScript==

this.jQuery = jQuery.noConflict(true);

(function($){
  "use strict";

    var i,_el, p,parent, c,child
      , d = window.document
      , opacity = 0.2
      , premium = 'a[href*="/.premium"]'
      , parents = [
          '.HeaderTeaser',
          '.teaserTitle',
          '.article_highlights_li_padding',
          '.cont_details',
          'li',      // "li" needs to come BEFORE "article", so as
          'article'  // not to grab articles containing premium bullet points (li elements)
        ]
      , children = ['article']
      , directs = ['.js-ribbon-item']

      , isDirectLink = function(el){
          if (   el.is(directs.join(','))
              || el.text() == el.html()
              ) return true;
          else return false;
        }
      , isFullPageArticle = function(el){
          if (   el.parent('main').length
              || el.find('header.art__hero').length
              ) return true;
          else return false;
        }

      , hover = function(el,op) { $(el).animate({'opacity':op}); }
      , hoverIn  = function() { hover(this, 1); }
      , hoverOut = function() { hover(this, opacity); }

      , fade = function(_e) {
          _e.css('opacity', opacity)
            .hover(hoverIn, hoverOut);
        }
      , applyFade = function(i, el) {
          fade($(el));
        };

    $(premium).livequery(function() {
        var _e = $(this);
        if (isDirectLink(_e)) {
            fade(_e);
            return;
        }

        $.each(parents, function(p, parent) {
            var _close = _e.closest(parent);
            if (_close.length) {
                if (isFullPageArticle(_close)) return;  // exception for article-pages
                fade(_close);
                return;
            }
      });

        $.each(children, function(c, child){
            var _child = _e.children(child);
            if (_child.length) {
                fade(_child);
                return;
            }
      });
    });

    // quick n dirty
    $('#top-prom-ban,.footer-ruler').remove();

}(jQuery));
