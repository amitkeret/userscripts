// ==UserScript==
// @name         Reading Mode
// @namespace    readingMode
// @version      0.6.3
// @updateURL   https://bitbucket.org/amitkeret/userscripts/raw/master/readingmode.user.js
// @downloadURL https://bitbucket.org/amitkeret/userscripts/raw/master/readingmode.user.js
// @description  Poorman's "Reading Mode" - delete elements from the page.
// @author       amitkeret
// @match        http*://*/*
// @grant        GM_addStyle
// @grant        GM_registerMenuCommand
// ==/UserScript==

(function() {
  'use strict';

var dq = function(q) { return document.querySelector(q); };
var init = function(){
  if (document.getElementById('readingMode-buttons')) return;
  var style = GM_addStyle(`
:root {
  --readingMode-main-bg-color: rgba(0,0,255,.3);
}
body.readingMode-choosing * {
  cursor: crosshair !important;
}
body.readingMode-choosing #readingMode-buttons * {
  cursor: default !important;
}
body.readingMode-removed .readingMode-delete {
  display: none !important;
}
#readingMode-overlay {
  /* "global" variables for this userscript */
  --size: 50px;
  --font-size: calc( var(--size) / 2 );
  --margin:.5em;
  --padding: .7em;
  --border-radius: .3em;
  --overall-size: calc( var(--size) + var(--font-size) + 2 * var(--padding) );
  position: fixed;
  box-sizing: border-box;
  top: 50%;
  transform: translateY(-50%);
  left: var(--padding);
  transition: left .5s cubic-bezier(.5,0,0,2);
  z-index: 99999;
}
#readingMode-title {
  font-size: var(--font-size);
  font-weight: bolder;
  position: absolute;
  top: 50%;
  transform: translateY(-50%) rotate(-90deg);
  lefT: calc( -1 * var(--size) - var(--padding) );
  display: inline-block;
  color: white;
  text-shadow: 1px 1px 1px black;
  white-space: nowrap;
}
#readingMode-buttons,
#readingMode-buttons > button {
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: var(--padding);
  border-radius: var(--border-radius);
}
#readingMode-buttons {
  background-color: var(--readingMode-main-bg-color);
  padding-left: calc( var(--font-size) + var(--padding) );
}
body.readingMode-hide #readingMode-overlay {
  left: calc(var(--overall-size) * -1 + var(--padding));
}
body.readingMode-hide #readingMode-overlay:hover {
  left: calc(var(--overall-size) / -2);
}
#readingMode-buttons > button {
  position: relative;
  margin: 0 0 var(--margin) 0;
  cursor: pointer;
  border: none;
  width: var(--size-smaller, var(--size));
  height: var(--size-smaller, var(--size));
  font-weight: normal;
  background-color: white;
}
#readingMode-buttons > button.smaller {
  --size-smaller: calc(var(--size) * 4 / 5);
}
#readingMode-buttons > button:focus {
  outline: none;
}
#readingMode-buttons > #readingMode-toggle {
  margin-bottom: calc(var(--margin) * 5);
}
#readingMode-buttons > button:last-child {
  margin-bottom: 0;
}
#readingMode-buttons > button:after {
  position: absolute;
  top: 50%;
  left: calc(50% + calc(var(--size) / 2) + var(--margin));
  padding: calc(var(--padding) / 3) var(--padding);
  border-radius: var(--border-radius);
  color: white;
  background: rgba(0,0,0,.5);
  text-shadow: 1px 1px 1px black;
  opacity: 0;
  transition: all .2s linear;
  transform: translate(calc(var(--size) / -2), -50%);
}
#readingMode-buttons > button:hover:after,
body.readingMode-choosing #readingMode-choose:after {
  transform: translate(0, -50%);
  opacity: 1;
}
#readingMode-choose:after {
  content: 'Choose';
}
body.readingMode-choosing #readingMode-choose {
  background-color: tomato;
}
body.readingMode-choosing #readingMode-choose:before {
  transform: scale(1.2);
  transform-origin: center;
  animation: readingMode-blink-animation 1s steps(5, start) infinite;
}
body.readingMode-choosing #readingMode-choose:after {
  content: 'Choosing...';
}
body.readingMode-choosing #readingMode-choose:hover:after {
  content: 'Done.';
}
#readingMode-toggle:after {
  content: 'Remove';
}
body.readingMode-removed #readingMode-toggle:after {
  content: 'Restore';
}
#readingMode-hide:after {
  content: 'Hide';
}
body.readingMode-hide #readingMode-hide:after {
  content: 'Reveal';
}
#readingMode-reset:after {
  content: 'Reset';
}
#readingMode-destroy:after {
  content: 'Destroy';
}
.readingMode-delete {
  background-color: var(--readingMode-main-bg-color);
}
.readingMode-delete:after {
  content: attr(id);
  position: absolute;
  top: 50%; left: 50%;
  transform: translate(-50%, -50%);
  font-size: 20px;
  font-weight: bold;
}
@keyframes readingMode-blink-animation {
  to {
    visibility: hidden;
  }
}

#readingMode-buttons > button:before {
  --fassize: calc(var(--size-smaller, var(--size)) * 3 / 5);
  content: '';
  display: inline-block;
  background-size: contain;
  background-position: center center;
  width: var(--fassize);
  height: var(--fassize);
}
/*
images from http://fatopng.com/fontawesome-to-base64
used: color = #000000 | size = 50
*/
#readingMode-choose:before {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAH60lEQVR4nO3cW4hdVx3H8c8ehhCGEMM4hBCGUCSEEEIegtYYglSRguKDSB4Ea8FabQ3VB/FBRJQgQUQKSgl5KFKlqChqWtKi8UIsCtHaqm2jtvWWSK1pzP3a6XRm+/Dfkw7Tvc9tX87O9PwO+yU7s9Z/fc866/L//9dKUqmR6tfYsA14o2gEuiGNQDekEeiGND5sA4qUSMYxha1YX/Df5vECjuFsKp1vyLz+lbb0g504glmkHZ5reBhbh21zx/YM24Bco2JIO9wF8OJnDt/E2LBtL/okWcNapUQyiZf0N7Sdx5vTlg4fbZ0MJ/Q/f6ypw5Cq1FbQy04j0A1pBLohjUA3pBHohtQ60IlkTKw6BtGgf1e7WrMFTySrsA2b8Y4Bi/lSInkGz+JYKr1alX1lNfQNSyKZwvvxXgF6g3I98zKO4094FD9LpWdLmllaQwGdOYwmcRs+hmmsUu1QNu816AfwQ5xPpa9WWEfvanrPj3W4E38UPope/Rllnjkcxe2YGoavo9nKYmj4Ps41BHjpcwbfxuZlCRorsLvhXlz0zOJ3Yl4YXzagsRp7caEPEP8bEOJLfXyRZ/A5rLrhQYsJ78uY6RHwUXwNdw8I+kP4Bp7sEfg1fB5rbljQwm25r8ee/Dd8GpvE0m56QNBjYvWyJeutJ3r4m3P4Qt09uy7Iq/CVrMd0auSJrEettSg6Ugb0ojLGsnL24T9d/u6KGN4mbhjQWQM/0QXyLB7D++SEn6oAvcSeD4phqVP88YpY/tUSDqsD9C34R4cGzeBHYqmX26gqQS+C/VYc6gL7OexqPWhsFEHVooloFvdbMlTklLN+AMizXcocy8p9sAPsObFtv6m1oMUktk/xCmNGbFa6joNYqf8l3p97+dmL5ebBDrBnxHi9sq2gb1Y86Sz0lE19lNfrsjDFJdzTR9lb8PMOv7wT2N460NnP8sEuvW1nLz1uUZlT+CyeEUvEoudJ4TtZ3ae9t4gxucjmB/qxtynQuxSvl6/htqoMrvKDOxSvji5gR1V1lXZLJpIV+LBiH/Iv8IOy9dSk7+JXBe8m8JGsfaVVhf93C3bIj9ZcxH2p9JUK6qlcqfRlsWW/nPN6XLRrcxV1lQKdxfd2FRgzL3ry42XqaECPCzvzUsk2Y1fWzlIqW8AavF0sx5bqNA6n0vMl66hVaYS5DiMv3DUh2lc63aws6ClsL3j3d/yhZPlN6Qk8X/Buu2hnKRVGwRPJahF2Wqv4C9kmPG5LNS969IZEMr3k3UW8KBLHG4vfLYpTrheblsUaE/bOe31bN+PWRLKuoOh5nMLJVHqx0ICCZc+0CGieMZjPodtW+QjeraF8ZtGh3qO3xPZBnjPYj/WFNhQYtlfvu7JBn6OYbAj0ZFZfne2ZwRf7Bd1px1TVM6cmT1lOe3ZpJlb5XJENRWPv2oJ/r1Jjoqc1oUnNpL8Vcmtd7t1y1Qh0QxqBbkgj0A1pBLohFYFuasf2hqmnCPSzNRmyWFfx7wbqIc6LN5GUXsitCPQB4ZOoS/MiX/l4jXUs1j/xY/mu0Kp0XmzDc5WbiJ5IJkS25SeF06jI+bTgqMnTVfkO9dMC8v2p9IVCsytW5ty6S2S15tm8SnGU6KziYeFV4fnbj0fSCCa8XiW3tjfLjxXO4TtqTLGqeIs+kdmbt02/gJvL1lF21XFK3JWxVGPChbqlZPlNaasscyrn3dOinaVUBeiiUNVG7Mz8wK1VZt8OYW+efmvYoNM4XnZEOPKXaiU+qrgBbdEmcWApLxz3Ih5LKzhGV8WG5Qn8peDdNnyggjrq1G4xdOTpmGhfaVUB+qRI98qbbcfwqURSSci+aiWSrWJllcfhZRxKpScrqayiWXu1SN0qcog/inV9ljmFz+icEnYOv9dnSlhW/rTO1wk9pcJTABUVk9I5veqSOJvSc+hKJDl2OzGweAm2p4+yp0TizJWC8q6oOI2tuoIiunCoA4yFU1Bdj5ypN213XMREO511PKjig59Vgh4TkeZO8cZL2KPLRkYNiehZuRMiQ7WoJ6f4q8g0rTRCX2FR13vLHp1PYl3CV3UKzVd8tCIrcwPu6wL5nBjvK0+DqLi46xPj/i5ALons/9xk76pBiw3Jw10gp7hXTcfgaijyOqif6BzinxPj6h3Zl1PH8bdJ3COGs262HNTnymjooLOGvkWcvuqWiDMnsvZvx01iIixzoHOl2I3eLZaG3fI5ZoRDaUNdLGoFncHejO/1CGkGvxQHju4cEPRusYz8td5Tvx7Axjo51A46g70hg91rptCsODw/COj/9gF4RtxnWjgp31CgM9irxS7vXwMCrPKZE2fP92joZoPGQGewx3FrNjz0uuOr+rkiJul3avhm3sbvVEokG8VY+nExYTaheZEYfwAPpdLjDdV7XcO6vGqF2P3dJe7YWKv6O+vmRdzypLje51s4lQ7p4NJQr2PLDuFMC5/1u4RfeFq+E75XLaQxHBOnYx9JpXmBiUY19HvvFpRdzr1wweDbxEamX31drJ2fx9Npp6MODas1oBeU9fJNwrnTr96Ey2kLb0VvXe5dBikvH6QXtRIyLQS9XDUC3ZBGoBvSCHRDGoFuSG0FfVX/yeOtPtzfVtDnFV9Ykqd5PFSPKdWolaCztfBe/Eb3nv0Kfop727qGpoU7wwVlWZ5rxbZ8nfxOMe81v8bpEeiR2jl0LEeNQDekEeiGNALdkP4PX9b3TLvwCu4AAAAASUVORK5CYII=);
}
#readingMode-toggle:before {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAFR0lEQVRoge3ZXYhVVRQH8N+5DJdhkEFiEBlkCJHBJCIkLCIiooeQiIiIsJeg6CFCokgiJIiICJGI6CkiIkIkIiQqSihIoocQMxFTsTL7klKzmmSaxtPD3tfZ99x9Zu6H4BCzLoeZ2Xfvtdd/fa8zRan0f6DGpRbgYtESkMVGS0AWGy0BGZQKxWihuLlQLLsY/C6lRR7HbuwrFA8VihUDcSsvwQercQJlfM5hJyb65Tk0kBZQKBoYi8KtwbXx5+W4DMM4jz9wGkewCuMJm2Gsx2jfggyo2bV4SnCRk5g1p+Ven+fR6FuWPoQfwQ3YgTMDCt96DmLVIErtybUKxTgewz3aXSNHv+Mr/IRJwXVy9DdewS+9yNJBPVjhDhzAjHrNzuIonsRKLBP8f4UQ0Lkzn2Asc+dt2IChi+JaUZBn8GtFgDOYrqztjZc3KjyuqgEygzsyd05gX1TKfVV+PQMRMs9rFSFOCa7wBM5WQFxdseCbgv+fqrHeTiyr3NkUEkhLSWfxNJb3BSS6xrvag/kwNgnp9vVkfUqImwaWRwtOzeOCJX7GLTUe8HC0eMr/BQz3BETI8zsTENP4EOuisBM4nlz0WdTkkBAfC4Eo8RKaNfc3sRGHEhnOxTOXdQUkauR17UHdVnWFOEgttTWuj+HHLkCcxGTm7nUtV4sKu16IlbQDeG5BIBHEk4l/zuAtrKzsu7Mi2J1x/aYuQEwLMTBU4TkmxNSaZK0hJIr9ieL+FBLA0HxAbhd8t3Xpblye0dymGiAPdgFkL1ZneN4tBPZkZb2BG3Es4XEUG7JAhFx/KNl8DOtqfHhjRbgDQotxeAEQszLpVOjJvhCyWw5kIyr5z4TPB6lV0s2PJuY7FbWezd+4UmcN6eb5HCMVXkN4RPD//RivubOJbdrd/rrW9+k8coW5+eRLvF8qz8vTb0L70QudxvZS+XdlfRL3C/H5pdAld1Cp/Adv4Lu41BCSw4U/WnRIaLcJGr85tuh1Qn2c7O+GPopPle6P9/2F3aXyr9zhQtEUiuxEXDqPry9sqMTIUe1+vyZn5rh/A77VnUtNiVW/wmO9ueTyGVbU3NXALeaKZCtGLtSh6oG7tfdUu9T4bNy/1cJt/Cxe1hngo0L7MysE8W0LKO1gwvMwrmnbUzkwIrQXrYCajpd1dKeJFd8xf0d8COszZzcKSWUq3pltP4Rp8/NEYWeFRFRfR+LBYe3tSYlXVYpisn8isz+1xpaMNUawJyrqWYzWuNP6uC910aezctQIt1p7wzgdNb+6KlTcP6k9vtJatDwj4KboKptrQAyZa0/SXmt7ld+8QCKzVXivoul9QmFqVvZuylhkCg9k+C7DXTK9VvL9Zu0dxjkhznrrfhOma6LbpMXvZNTMRNTcuJBxqtb4MOeO0SI5qzaFvmqHuQpext+fUxOnXQFJNPS8zuHomJC1tuuc/s7JzBo1/JtCVtqmcwo9jodywHsGEi8bEQanw9oz1Iz87LFLZtaO1mhGfiuFuXyHUI9mK3z3CM1idmbpC0giyHjU3ImM8K3nhKQHSs6ujb7/klDMztacPyhkunlH24GARIGGhYyyS2f9mMWL1aCMlnhbffGcEQaybTFOunpzMhCQKNioMHBVBfpW5kVbdKEqiGkhZb8d4yA7wnb76ffd7zrcWlk7j1dL5Q/pYqEYw734XuhsjwjN3l58g+9KZbbj7Yn6tMhac5W5peE9BnibPuinXyCtNylbhMI1JYy5fb+EHvRTRMH6pvgPmivxaan8dyBmg8gxKJDFQkv/DF1stARksdESkMVG/xsg/wEAHSm+MAXk+QAAAABJRU5ErkJggg==);
}
body.readingMode-removed #readingMode-toggle:before {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEcUlEQVRoge3ZX4jUVRQH8M8syyLLFouIhIhtIiIlEiEhIv17CBFZRKSHigjpQULEICJEBB9CJKKniOgpeoiI6EEi0qIiKKLStcX+YCIlUuGWmO7qto2/Hu4d5+5vf7/ZmZ2BBpkz3Ie598y953v+3XPuVDKZm4H6/m8BOkU9IN1GPSDdRj0g3UY9IN1G/Z3YpKLSJyhlMe7BatyOJVgU2a5gAmfwPcZwLZP92xEZ2ilRKiqLsR73YyvWYKDJn0/hGxzBFxjLZFMLFiZr8SNo/lY8gWO4gCqyBY4qzuEdPIzBVmXKZK0BwTB2YrwJASfxA77E2RZAHcMWLOo4kGiFNXgLl+cR5nKi3TuxAhsw3YKV/sCLWIq+jgARgnU3zucOu1Qg3Omozf6cElZFCzUS/mqOp4qT2NYMmPlALMELUej0wA9wCBeT+bPYEn/XLySBQ3gfp5qwwi94Hicwk8yfE9y5oas1ArEsukg1J+yuCPD1HLid6onguTjXStBPY290p0P4M1mbxGENEkEjEG8kmqniK2yK2l6On5KDvsVQXNuds2Ar410hoQzikWilmiInsb8MTBGIIbyZs8R7GEl41ufWDySueG6BIGoxtjyJrQ1RSall9imImTyIAcFPa0E8I1xYIzm+bTkBtsf5TW2AyISMtyp31nohxmqKu4jRPJg8kFEh9dU2PpYHEfkeLQGyq00gM1hdcN5Gs135JNYWAhHqpPSiO5NnzgHOchsfFi7AdoBcylskcbNR9dirCvHUXwRkT2K+C0KwFeZvrNPaBdfsGMeykjMHcFA9G85gQ209LePvUi/rv8PRTHZdMU3gx5K1dmgMfxctZLJ/BCv8Gqf6hMrhxpcanUJN8LuxNZbnRTSBowl/J+gKPs5kV4oWKyoDeAwjceq60A4EahAjZ7GuyMxJNjmjc271OZaUnNWH7ep1XnmMJGk1zVqfKAi+hP9Z7ZXwtXERmxuAeEC4Y+bPWklA7VMP5FpZvbLkkGG8qvVyJA9iLwZKQNwrpN70HplTSBYJV3SzH1GQ3yP/UryyQMtMRhBzCsLEEidy/PvzIAqBxE2Kaq0TuC/1yxz43WbfwI3GtBAToyWWGMTjQuuQ1loHMFQocwP/L6t+9ygo3NTT4UF8bXYpnmr0w7hH2X1xG14yu0W4Gudaq36TTYv6kWmhH9lUAqhfKOVHomvsiGNjVM5QoWuErLlDCORUCefxVCMQ8wKJByyKGsx3iL/hNeH5Z467NfuJ+28R0mm+/B8X0m57HWJyWK1nf9vsnr0av3+Gp6PWV5T6cdhnWGh9HxICd9zcrHcBL0c3a6pnb+ldq6IyHDX0DNYWsEzgZ/wex19CXPThFsFVbxNcbKXggildx6cRxEeZ7FrTwi3AFWpafVK4Y9KWND+quVHGc15wrc0W+K7VqZfGBwU/b/Wl8bj6S+PxrI2XxraA3Nik+O33jvh9UHCZKcH1TguV8ximsm54++0mumn+VugB6TbqAek26gHpNuoB6Ta6aYD8Bzo9aJMdTZNsAAAAAElFTkSuQmCC);
}
#readingMode-hide:before {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADUklEQVRogd3aPYhdRRTA8d9dHssSlmURiyghhMVCtggWy2IhlmKRwiKIgqQSFQQRBUEQGwsRC9HOSgQ/CpEUViERCRFEIxIJIn6wFiliDJso0Rizmx2LuQ9e3rvv5X7MvH3kPKa5986d83/nzJkzZ24RBLeDzO22AqlkKiCFolcoFrKOkdO1CsU8DuFxLOEM3g3CRvLBQqYfFvEK/kUo2xbOYg1zScfLBLGMl3BlAGKwncZ6SpgcEPN4ARfHQPQt8wVWZxIEC3ge/0yA6LcbeAsLMwUiTubncLkGxKCLLaUYP0n4LaPTEbxczo+6sp1ifOh1fUEJ8QRe0xziBK511QHdXEsMsU9hU3136k/2o1hJ5todIHolxLmGEAHHJYxYrUHE1OawySF2XKT6OjVEKxDsESf2hRbudCwHRGOQ0hJH8FsLdzqF+3JAtAF5COdbQJzFWi6I2iDiiv1YC4gbOJnTErVBSnd6FL+0sMRprOeGCEKtBfFBvIn9NZ4dlD/xMRSKtYZ9q+QaNoJwterm2I1VuWI/jLdxoMXAO7jeot+k9/2Bp3EiCDs33Z3gUo/gR83dKXf7XkUIHwexrt2cmEa7gmeGdR7JfgvFXryBe0aMOxuyiLuGL1al8fdjNbs63WRE7yqQRQnS+2lLFcgGLk1bkYayM3yhCuQbfCTh7i2xbIslpptkBCQI22JR4ANp14FUcgk/j1ydsI7sw3v4z+6H3H7bwodYHtZ3Ysm0UCzjdTypeQC4Lrrp3w37jZOrYu72TmWaUiNpvFPMmbY0X7helahudUs9az3ECj5pAbMpFuzmZwKkhLkD73ewzOJMgJQwe8UyTlOYv3JbpnkH7sVn4u6vCcwFsXyUBaZdJ+4uLdMUZlOsDycPAO07xh3jsRZudlkM50kt0wVkDgdLmKaWOSfWi3u7DjIAs4LPW8IckujUKs2/ETdhJ1vAfIo9KXRIdTy9gRfxpYoUe4KsiEd1nSUJSIgVje/wLL5VH+Z3qbYLKcw65GarYsX9Vm52UZzwszNHhkDm8AC+mgDTT1tG0vGZASlhemJJ6YcKiC2xSpPkEDQryADQQfF06ry4EP4kU2qf9VsUKBT7xE82lvArzoQx9dtO4+QGmZbcNt9r/Q8sKpfk/UgqGQAAAABJRU5ErkJggg==);
}
body.readingMode-hide #readingMode-hide:before {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADUElEQVRogd3aP4gcVRzA8c8cx7IsRzhEggaxOI5wBLkiqFhYWFimELESSREs/AMRVIRYCBZWQcRaEIJoTGER0OLQCEFBozYiURQVQWIMovEvUbN3Y/Fmw7qZuZs3997e6m/4NTPD/N533++935+3Ran0f5C5nR5AKskKUij6hWI+p40rtnK4VqFYwoNYxa84jtdL5d/JjY2kTHxhPz7BOspKL+FJLKS2d8VuQoA53IwzYwDj+huewOKsg+zDKVxuACnxAx5FbyZB0MfzE+7UpH/gMPqzCLILH7aAGOnFCibZmkm5/Q4j3l3EERwsFL0UxlOB/Im3xcFch2dwbxKYZD7KEl6z+WKv0x9xPwbbsp8KpIJZwVokSIlvcQjzMwEyBvOedjvYuF7A3ZibCZAxmLUObnYB9+mwNWcBqWBW8U4HN/uqgomamWwgFcwo74qFOYc7ZwakglnF6Q5r5hzu0TKdyQ5SwdwqLvKP9PMKZks3ayx6CsVAiA39pnci5TiWhajeVvbiKL7Hu5u9eFVhVVV0d+AF7Ja2iux1/N6XeARvlU3FWY0b3ISPxbtBbj2LA63XCB4QiqCdHnjTmtlfB1I3zddjoXb6dl724rlCsWfyQR3IrLeIVnDb5M1ZH3SdzGMwebMOZCP/WLYlP+ObyZt1IJfEFUjTlCFexfuTD+pAvsBPuUfUQYZ4BUdL5dU/dM32u4iXxafgOfUvHMMNTXGktmVapSeHcYuahdVRFoScK7Y+H+JFHCmVzZ4ypaSxj6fEB9rLOIHdW9qYAkRP6C5ejIRYF5oZS63sZIbYhaeF7mIsxEu4trWtjBB9PN7RnU5iT5S9TBA9PCQ0rWNn4g2sRNvMADGoZqLLmjhpky12aiBjM/FLB3daw42dbSeGOITzHWbiTaFJ0ak5lwxESHXuEjofsRCnhFq+M0RKkIGw58dCnMZyijGkqkd6QselrWwIXZHH8HWKAaQ6Ax8KLZs2soGP8DA+LZVp6p9ErjWHg8JZx1budAb7Utj91xiSfYhrhBOopnRkXThuuN02F3ZWkApmEc+qr2XOCml858OcqYFUMKOU/TMhup8Xgt1qDoDRleu/KAMhwC3jd3xQKr9LbmjcZg6QnZD/Yl+rVv4B+euRDhsaEMgAAAAASUVORK5CYII=);
}
#readingMode-reset:before {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEwElEQVRoge3aT4hVVRwH8M99PIZhGIZhGEJikCiJEJGQsCiRQVyEBEqLiFoFQbUxqRZREbSIFhHRolq0iBZSIgW1KAoJLDRKLEzLMq3MKJUcR52xUWfmtvjdGd978/7fO2ThdzgMvHvvOed7f7/z+/3O99wklfo/oPRvT6AolIvuMJH0YihrfdkY05jCOE6m0qnCxy3CtRLJIEZxK27EMPrR4zKRiziPv/ADvsRnqXQs9wTkIJJIBrAc92ODmHyvmHwrXBQWOov3sA3fp9LxFmPOWZuw7PT8xbTDP+Eua/AGJpEW0M5gO9ajv8G4A3gCv+AnbKq63iGJYTyDHzFTEInKdhQvYlnNuGU8h1MV925Hb8dEMIL3cW4RCFS2v7Ers3pJuOtTmdUq7/sIg20TET4/ir2LTKC2ncKDeKnBy/u4bSLZG1mHPYvkSu1Yp9E6rCLSKo+M4GWs0F7ynMXP2I19OI4JYdUh3IDVWetro7/eNu4JNLHGEuGHrd7aDE5gK9ZiMJtAGaUa6/ZkBJbhSezHhTbGaGmRRiT68bzW4fWcCAAbKifdQQBZildwbLGIrMORFh1NipC4tBsSNS/tPnxXKJHMNbZqvrgnsQV93RKoGXNAJNhLRRJZ38Jvz4i4XhSJocyNa/NEbiLbmzx8CW/j2oJI9GQkThe6RkQ0+bPJw8ewuiASJVE7dWqJukRq88gmsUYa4Z1U+lWT620hkZTxMJ4W6yM35pNcIukT+4lGZfhZvFXEoCJS3aH5S2uF6ayhOlsPiuq2UQb/BIdzDFyJ8ziY/e8GU/ii8vn5jVVm7rVYaSGZKexIpYe6HHgBso3ZqFiXneIwPk2lE/P9zRH5r+N/o6JcJXKl4SqRKw3zmT2RlEQBN6hzBfKi0JkmWt7ZJrJ0MKL+LnECf6TS2flfKmqfFaJ+6WZvfkHIOHU1qS5rsVGNFZuDtWNVutYakRC7cbce3I3ru3h2ATLveEiUMvVQEvpA1Q9z6NPJZn8hxnAyx/OVuBm3N7m+O5VWlTdFLfbDeCSVHs/bUVa8PiDEj3qYFnuiKhRB5AAexdd5O8pcah3u0rgKP4Bva3/MS+Sw2FPsqIog3WMEjwtBox5mxQ524VFERZR4TGeR6rR4e10rKDVR6hqhozWLmnuxqt7zeSwyJaJKngAhkZQSyUq8KoSPRnOaENY4UPdqDouk4hjgWSzp0gol3IvPtVYcd2KkYV85iaRC49oj8sggelpMvixC/S14U6jurZLwOaxp1m8Rh6Fzk9qGQ/gwkewSAvZ5Ub6UhQsOiApiI27TnpA9JgLK7mY3FXmqWxZnisuxWRAZE2upR5BYonG2rodxvCbUm+ZRsQ3XOirUkxMNri9Wu4AXMNTWemtB5IiowcoiSR20+Ac+M0Ik3IJy24Gjgshm1SLyPmyoWair8a7u1cF2rLBTCIVNg0YzIqPi2HcG3+DO2s5EuBwRKuH+gkmcylxpeSeWqEekR1Sd94hyvGFn2b0jQrvdl1mokyOBOReaFGvwdaySQ+HPrWslkn5Rcm/ETaLUGBJRqtflvcPcJxzjotz/VRxDf5BKf881CQUKdFnlOozrRJgddvmjmlkVH9XgN7FVLezjmqtK45WGfwASta3vNliZQgAAAABJRU5ErkJggg==);
}
#readingMode-destroy:before {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAE7klEQVRogd3aS2hdVRQG4G+HUEIIIdQgIkVE21IkOChFRKT4QkRERdSKj4EiCipF8TUQBXEgDjoQQQUfRZz5HCi+8ImKVBCxaLXVatHiq1WrrRpjku1gn+BNsve5597cq9D/cibn7LPW+vfj32uvc0MUHQwY+L8D6BX6TiQIw0FY1m8/fSMShNVBeAwfYksQbgnCWL/8iX34YQXeRmy5ZnA3Bvvhs18jcibWLrg3gAtxZD8c9ovIKgxn7o/iqH447BeROrt98XnQyO9gNy8FYQTj2BfFfUsNIggDOLSK58coTnVqo2MiQViPGyvHe4OwGc9347yyN4aNOLWKZ2sQ7ozi9x0Z6kBSR3A99pgvq/tx/oK2mxa0mbv24IyWdoPYLElzq0x/gPU6kOqmJAZxB34tBPhWl0QmMh0zd32C03q2jwRhCNfgBkk+c+hWUleglL4cg/uCsK5aQ7WobVAZOB+3oy692NbOUQFfYrLm+Rrci9XtDLVjerhEYrymzT480M5RAV/iqTZtjsPNQagXppp1MYxH5Odv6zw+14JFqeEaqdqO4S78UuNnBmd3tdilfOm7GuNf4ZTCu42JVO1HcBP+qvH3Gg7raLEHYRQbpL0ih29xK94sPO8IUTyAB3E/SvvROpxeWvilNbISZxWeT0kL8JkoznYUcQ0qMpvwKnJ2R3GBguiUiJyO5YVnX+DhKE53Fmp7RHE37lNWsvWSAC3CIiKVOpxTMDSNTVH8uYs4m+IV5Sk7KnXyIuRG5Cjlw88OvNNhYB2hmq6PSZ2Ww8m5mzkiqzFUMPI+miRzddOuyZTcil2FZ2uqbGMeckRWyhOZwidR/K1BIDtxIHP/t5oAW7EXHxeejcjMmByRQ+TT+z8k2W2CF6ReXYgnNCNyQHnkB2WUKxfwsDzBaalH2yKKu4NwCW6TihCTeBKPNlS7qRpfA9KozEOOSEmSZ+X1PYso7grC1ZXT6Sj+0fTdFn8lLIoxR2SyMrKw8aB8ZaSISoEajWIHvmalaT4Pud7fI68sw9L54b/AaI2vaSzax3JEdsvnO0NYlZO+PmBMUs8cpvD1wps5Ittkhq7C8QopQo9xjHSoymFHlZfNQ47IDmWZnZAOOv3GZcpH4DdyNxcRiams83rByDJc28/PBEFYi1MKjyeVYiscjNYqV0z+wnX6UFWXzj/Pml8ear1exHjjE6KUwj9bMBbxKU7oMYlB6ZRY6sDf6zqwZHQA5ynXnGbwkbQoB3pAYggXScW+Uue9iyOKNmqMj+LxGsMR7+GkpUwzaeffiB9q/Mzg4lo7bZxMaF/d2FkN+VAno1ON+gqpUlPnI+JljCyFyACuaOBoBltwVTXdsk4re+PSfnSP+irN3PUBjm3XMaFyUERVLb+tGv52sjstpe+fYbu0Ax/wb+p9tLTRTWj2CW4vLsdLsV3W3ME8fgh/at+DrTK9X1Kh/ZLq/N3B+9/hUg3XXyMiFZkxaTqU5LGX13ap5ryscXxNG7aQuVL7NdPtNfdt5ISmI9EVkRZCE1Kl46ceEvgct2B5VzF181JFZkT6bv50Nf+7JfGNVGE80RI217aqVYeqDjv37XyDlOwdLh3ChiS1mktMZ6WzxKSkZNvwnFSo+D6Kdd9J2seyFCKLjKUq5YR0KDpCqsgMSST240epivJZFHf1zLEeE/k/cdD8YeAf8YQgy2o7InAAAAAASUVORK5CYII=);
}
`);

  var overlayHTML = `
<div id="readingMode-title" class="readingMode-ignore">Reading Mode</div>
<div id="readingMode-buttons">
  <button id="readingMode-choose">
  <button id="readingMode-toggle">
  <button id="readingMode-hide" class="smaller">
  <button id="readingMode-reset" class="smaller">
  <button id="readingMode-destroy" class="smaller">
</div>`;
  var overlay = document.createElement('div');
  overlay.id = 'readingMode-overlay';
  overlay.innerHTML = overlayHTML;
  document.body.appendChild(overlay);

  var readingMode_reset = function() {
    Array.prototype.forEach.call(document.querySelectorAll('.readingMode-delete'), function(e) {
      e.classList.remove('readingMode-delete');
    });
  };
  var cbs = {
    'choose': function(){
      document.body.classList.toggle('readingMode-choosing');
    },
    'toggle': function(){
      document.body.classList.toggle('readingMode-removed');
    },
    'hide': function(){
      document.body.classList.toggle('readingMode-hide');
    },
    'reset': readingMode_reset,
    'destroy': function(){
      readingMode_reset();
      document.body.classList.remove('readingMode-choosing');
      document.body.classList.remove('readingMode-removed');
      document.head.removeChild(style);
      document.body.removeEventListener('click', bodyListener);
      document.body.removeChild(overlay);
    }
  };

  var bodyListener = function(e) {
    if (!document.body.classList.contains('readingMode-choosing')) return false;
    if (e.target.classList.contains('readingMode-ignore')) return false;
    if (e.target.parentNode.id == 'readingMode-buttons' || e.target.contains(document.getElementById('readingMode-buttons'))) return false;
    if (window.getComputedStyle(e.target).position == 'static') e.target.style.position = 'relative';
    e.target.classList.toggle('readingMode-delete');
    e.preventDefault();
    e.stopPropagation();
    return false;
  };

  for (var button in cbs) {
    document.getElementById(`readingMode-${button}`).addEventListener('click', cbs[button]);
  }
  document.body.addEventListener('click', bodyListener, false);

};

GM_registerMenuCommand('Reading Mode', init);
})();
