// ==UserScript==
// @name       Keyboard shortcuts: Wikipedia
// @namespace  http://bitbucket.org/amitkeret
// @version    0.1.4
// @updateURL  https://bitbucket.org/amitkeret/userscripts/raw/master/keyboard.wikipedia.user.js
// @description
// @match      *://*.wikipedia.org/*
// @copyright  amitkeret
// @icon       http://chinwag.com/files/images/logos/ctrl-logo-1.jpg

// @require https://cdnjs.cloudflare.com/ajax/libs/mousetrap/1.6.1/mousetrap.min.js
// @require https://cdnjs.cloudflare.com/ajax/libs/picomodal/3.0.0/picoModal.min.js
// @require https://bb.githack.com/amitkeret/keypop/raw/master/dist/js/KeyPop.min.js

// @resource mtsc-css https://bb.githack.com/amitkeret/keypop/raw/master/dist/css/bitbucket.min.css
// @grant      GM_addStyle
// @grant      GM_getResourceText

// @require https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.0/mark.es6.min.js

// ==/UserScript==

(function(mt, mrk){

  var i,
      d = window.document,
      dq  = function(q) { return d.querySelector(q); },
      dqa = function(q) { return d.querySelectorAll(q); },
      did = function(id) { return d.getElementById(id); },
      dqc = function(q) { dq(q).click(); return false; },
      dqic = function(q) { dq(q + ' input').click(); return false; };

  var tocsidebar = d.getElementById('toc').cloneNode(true);
  tocsidebar.id = 'toc-sidebar';
  d.body.appendChild(tocsidebar);

  var highlight = d.createElement('input');
  highlight.id = 'highlight';
  highlight.setAttribute('placeholder', 'Highlight...');
  d.body.appendChild(highlight);
  did('highlight').addEventListener('keyup', function(e){ if (e.keyCode === 27) this.blur(); });

  var markInstance = new mrk(did('mw-content-text'));
  let performMark = function() {
    // Read the keyword
    var keyword = highlight.value;

    // Remove previous marked elements and mark
    // the new keyword inside the context
    markInstance.unmark({
      done: function(){
        markInstance.mark(keyword, {separateWordSearch: false});
      }
    });
  };
  highlight.addEventListener('input', performMark);
  highlight.addEventListener('blur', function(){ highlight.classList.toggle('active'); });

  // Custom shortcuts
  var shortcuts = {
    't t': ['Toggle TOC', function(){
      did('toc-sidebar').classList.toggle('active');
      did('content').classList.toggle('blur');
    }],
    't s': ['Toggle sidebar', function(){
      var sidebar = did('mw-panel'),
          content = did('content');
      sidebar.classList.toggle('ts-hidden');
      content.classList.toggle('ts-hidden');
    }],
    'g s': ['Goto search', function(){
      var search = did('searchInput');
      search.val = '';
      window.scrollTo(0,0);
      search.focus();
    }],
    'g t': ['Goto top', function(){
      window.scrollTo(0,0);
    }],
    'h l': ['Highlight text', function(){
      highlight.classList.toggle('active');
      if (highlight.classList.contains('active')) {
        highlight.value = '';
        highlight.focus();
      }
    }]
  };
  shortcuts['?'] = ['Display shortcuts', function(){ mt.KeyPop.get(); }];

  for (var shortcut in shortcuts) {
    mt.bind(shortcut, shortcuts[shortcut][0], shortcuts[shortcut][1], 'keyup');
  }

  GM_addStyle(GM_getResourceText('mtsc-css'));

  GM_addStyle(`
#content {
  transition: margin-left 2s ease;
}
#content.ts-hidden {
  margin-left: 0;
}
#mw-panel.ts-hidden {
  display: none;
}
#highlight {
  width: 30%;
  top: -100px;
  left: 50%;
  transform: translateX(-50%);
  font-size: 2em;
  line-height: 2em;
  padding: 0 1em;
  border: 1px solid #a2a9b1;
  box-shadow: 0 5px 10px grey;
}
#highlight.active { top: 0; }
#toc-sidebar {
  width: 300px;
  top: 50%;
  right: 0;
  transform: translate(100%, -50%);
  overflow: hidden;
  height: 100%;
  box-shadow: -5px 0 8px grey;
  opacity: 0;
}
#toc-sidebar.active {
  transform: translateY(-50%);
  opacity: 1;
}
#highlight, #toc-sidebar {
  display: block;
  position: fixed;
  z-index: 100;
  transition: all ease-in-out 1s;
}
`);

})(Mousetrap, Mark);
