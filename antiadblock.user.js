// ==UserScript==
// @name        Israel sites Anti-Anti-Adblock
// @namespace   amitkeret
// @version     0.1.5
// @updateURL   https://bitbucket.org/amitkeret/userscripts/raw/master/antiadblock.user.js
// @downloadURL https://bitbucket.org/amitkeret/userscripts/raw/master/antiadblock.user.js
// @description
// @require     https://cdn.rawgit.com/polyfill/Array.prototype.forEach/master/Array.prototype.forEach.js
// @include     *://*easy.co.il/*
// @include     *://*haaretz.co.il/*
// @include     *://*ynet.co.il/*
// @include     *://*tapuz.co.il/*
// @author      amitkeret
// @copyright   amitkeret
// @icon        https://getadblock.com/favicon.ico
// @grant       none
// @run-at      document-idle
// ==/UserScript==

(function(){
    "use strict";

    var loc = window.location,
        doc = window.document,
        sites = [
            [/easy\.co\.il/, function(){
                doc.getElementById('bl').remove();
            }],
            [/haaretz\.co\.il/, function(){
                var modals = doc.querySelectorAll('.modal__close--subscription');
                modals.forEach(function(item, i){
                    item.click();
                });
            }],
            [/ynet\.co\.il/, function(){
                if (typeof CloseAdblockPic !== 'undefined' && CloseAdblockPic instanceof Function) {
                    CloseAdblockPic();
                    doc.getElementById('colorbox').remove();
                }
            }],
            [/tapuz\.co\.il/, function(){
                doc.getElementById('divAdBlocker').remove();
                doc.getElementById('divAdBlockerBG').remove();
            }]
        ];

    for (var i=0; i<sites.length; i++) {
        if (sites[i][0].test(loc)) sites[i][1]();
    }
}());
