// ==UserScript==
// @name        Keyboard shortcuts: EverShort
// @namespace   http://bitbucket.org/amitkeret
// @version     0.3.0.6
// @updateURL   https://bitbucket.org/amitkeret/userscripts/raw/master/keyboard.EverShort.user.js
// @description TamperMonkey port for EverShort Chrome extension
// @match       *://*.evernote.com/*
// @copyright   Akrog
// @icon        https://raw.githack.com/Akrog/evershort/master/images/icon128.png
// @website     https://github.com/Akrog/evershort/releases

// @require     https://raw.githack.com/Akrog/evershort/master/contentscripts/utils.js
// @require     https://raw.githack.com/Akrog/evershort/master/contentscripts/keymanager.js
// @require     https://raw.githack.com/Akrog/evershort/master/contentscripts/contentscript.js
// @resource    help_html   https://raw.githack.com/Akrog/evershort/master/pages/help.html
// @resource    help_css    https://raw.githack.com/Akrog/evershort/master/pages/styles.css
// @grant       GM_getResourceText
// @grant       GM_addStyle
// @grant       GM_info
// ==/UserScript==

// make sure this runs once on the topmost window
if (unsafeWindow.self != unsafeWindow.top) return;

// all was defined in the global scope, nothing was exposed
// overriding the original, since there's no Chrome extension here
get_help_html = function() {
  // We only load the html for the help once upon help request
  if (!help_html) {
    help_html = GM_getResourceText('help_html');
    for (var i = 0; i < key_groups.length; ++i) {
      group = key_groups[i];
      help_html = help_html.replace('{{' + group + '}}', get_key_group_html(group));
    }
    help_html = help_html.replace('{{tiny_mce}}', get_tinymce_help());
    help_html = help_html.replace('{{version}}', GM_info.script.version);

    // add "tabindex" to be able to focus this element for keyboard scrolling
    //  - https://stackoverflow.com/a/23298292
    // hack to create pseudo-"onload" event for DIVs
    //  - https://stackoverflow.com/a/41685045
    help_html = help_html.replace('class="evershortReset">', `class="evershortReset" tabindex="0">
<img id="evershortDialogIMGtag" src onerror="javascript:document.getElementById('evershortHelpDialog').focus();">`);
  }
  return help_html;
};
GM_addStyle(GM_getResourceText('help_css'));

// visual reminder there are shortcuts
var help_link_button = document.createElement('div');
help_link_button.id = 'evershort_help_link_button';
help_link_button.setAttribute('title', 'EverShort Help');
// Yay for ES6 heredoc!
GM_addStyle(`
div#evershort_help_link_button {
  display: block;
  width: 36px;
  height: 36px;
  margin-top: 10px;
  background-image: url(https://raw.githack.com/Akrog/evershort/master/images/icon48.png);
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center center;
  cursor: pointer;
}
#evershortDialogIMGtag { display: none; }
div#evershortHelpDialogContainer {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0,0,0,.3);
}
div#evershortHelpDialog {
  margin: 0;
  padding: 1em 1.5em;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 70%;
}
div#evershortHelpDialog a.closeButton {
  font-size: 24px;
  color: grey;
  cursor: pointer;
}
div.evershortColumn table tr td {
  font-size: 14px;
  color: #4a4a4a;
}
`);
unsafeWindow.evershort_toggle_help = toggle_help;
var timeout = setTimeout(function(){
  if (null != document.getElementById('evershort_help_link_button')) return;
  document.getElementById('gwt-debug-AccountMenuView-root').insertAdjacentHTML('afterend', help_link_button.outerHTML);
  document.getElementById('evershort_help_link_button').addEventListener('mousedown', evershort_toggle_help);
}, 10000);
